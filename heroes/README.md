Guide to run the project : 

1.i used Xampp, Download Xampp and install it 
2.Open the control Panel and con the config button open the file php.ini.
3.Inside the php.ini you have to look for ";extension=intl" and delete the semicolon ,it should look like this "extension=intl" .
4.Restart  apache and mysql (stop and start again).
5.Now go into the phpmyadmin panel (http://localhost/phpmyadmin) and import the file "heroesdb.sql" to create the database and the table with data.
5.Put the "heroes" folder downloaded from the repository inside the C:\xampp\htdocs or wherever you installed Xampp.
6.Go inside the heroes folder and open cmd inside the bin folder (C:\xampp\htdocs\heroes\bin).
7.Run the command "cake server".
8.Now go to http://localhost/heroes/superhero and check the page.