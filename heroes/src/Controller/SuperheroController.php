<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Superhero Controller
 *
 * @property \App\Model\Table\SuperheroTable $Superhero
 *
 * @method \App\Model\Entity\Superhero[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SuperheroController extends AppController
{       
     public $paginate = [
        'limit' => 5,
        'order' => [
            'Superhero.Nicname' => 'asc'
        ]
    ];

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $superhero = $this->paginate($this->Superhero);

        $this->set(compact('superhero'));
    }

    /**
     * View method
     *
     * @param string|null $id Superhero id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $superhero = $this->Superhero->get($id, [
            'contain' => []
        ]);

        $this->set('superhero', $superhero);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $superhero = $this->Superhero->newEntity();
        if ($this->request->is('post')) {
            if(!empty($this->request->data['photo']['name']))
            {
                $filename = $this->request->data['photo']['name'];
                $uploadPath = 'img/';
                $uploadFile = $uploadPath.$filename;
                if(move_uploaded_file($this->request->data['photo']['tmp_name'],$uploadFile))
                {
                    $this->request->data['photo'] = $filename;
                }
            }

            $superhero = $this->Superhero->patchEntity($superhero, $this->request->getData());
            if ($this->Superhero->save($superhero)) {
                $this->Flash->success(__('The superhero has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The superhero could not be saved. Please, try again.'));
        }
        $this->set(compact('superhero'));
        $this->set('_serialize',['superhero']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Superhero id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $superhero = $this->Superhero->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {


            if(!empty($this->request->data['photo']['name']))
            {
                $fileName = $this->request->data['photo']['name'];
                $uploadPath = 'img/';
                $uploadFile = $uploadPath.$fileName;
                if(move_uploaded_file($this->request->data['photo']['tmp_name'],$uploadFile))
                {
                    $this->request->data['photo'] = $fileName;
                }
            }

            $superhero = $this->Superhero->patchEntity($superhero, $this->request->getData());
            if ($this->Superhero->save($superhero)) {
                $this->Flash->success(__('The superhero has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The superhero could not be saved. Please, try again.'));
        }
        $this->set(compact('superhero'));
        $this->set('_serialize',['superhero']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Superhero id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $superhero = $this->Superhero->get($id);
        if ($this->Superhero->delete($superhero)) {
            $this->Flash->success(__('The superhero has been deleted.'));
        } else {
            $this->Flash->error(__('The superhero could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
