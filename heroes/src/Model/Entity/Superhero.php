<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Superhero Entity
 *
 * @property int $id
 * @property string $nickname
 * @property string $real_name
 * @property string $origin_description
 * @property string $superpowers
 * @property string $catch_phrase
 * @property string $photo
 * @property string $photo_dir
 */
class Superhero extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nickname' => true,
        'real_name' => true,
        'origin_description' => true,
        'superpowers' => true,
        'catch_phrase' => true,
        'photo' => true,
        'photo_dir' => true
    ];
}
