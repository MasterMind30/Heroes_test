<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Superhero Model
 *
 * @method \App\Model\Entity\Superhero get($primaryKey, $options = [])
 * @method \App\Model\Entity\Superhero newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Superhero[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Superhero|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Superhero patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Superhero[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Superhero findOrCreate($search, callable $callback = null, $options = [])
 */
class SuperheroTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('superhero');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nickname')
            ->maxLength('nickname', 100)
            ->requirePresence('nickname', 'create')
            ->notEmpty('nickname');

        $validator
            ->scalar('real_name')
            ->maxLength('real_name', 100)
            ->requirePresence('real_name', 'create')
            ->notEmpty('real_name');

        $validator
            ->scalar('origin_description')
            ->maxLength('origin_description', 250)
            ->requirePresence('origin_description', 'create')
            ->notEmpty('origin_description');

        $validator
            ->scalar('superpowers')
            ->maxLength('superpowers', 250)
            ->requirePresence('superpowers', 'create')
            ->notEmpty('superpowers');

        $validator
            ->scalar('catch_phrase')
            ->maxLength('catch_phrase', 250)
            ->requirePresence('catch_phrase', 'create')
            ->notEmpty('catch_phrase');

        $validator
            ->scalar('photo')
            ->maxLength('photo', 255)
            ->requirePresence('catch_phrase', 'create')
            ->notEmpty('photo_dir');

     /*   $validator
            ->scalar('photo_dir')
            ->maxLength('photo_dir', 255)
            ->requirePresence('photo_dir', 'create')
            ->notEmpty('photo_dir');
*/
        return $validator;
    }
}
