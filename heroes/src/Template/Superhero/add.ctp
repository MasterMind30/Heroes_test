<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Superhero $superhero
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Superhero'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="superhero form large-9 medium-8 columns content">
    <?= $this->Form->create($superhero,['type' =>'file']) ?>
    <fieldset>
        <legend><?= __('Add Superhero') ?></legend>
        <?php
        error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);
            echo $this->Form->control('nickname');
            echo $this->Form->control('real_name');
            echo $this->Form->control('origin_description');
            echo $this->Form->control('superpowers');
            echo $this->Form->control('catch_phrase');
            echo $this->Form->control('photo',['type' =>'file']);
         /*   echo $this->Form->control('photo_dir');*/
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
