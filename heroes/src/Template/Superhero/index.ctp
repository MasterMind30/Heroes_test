<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Superhero[]|\Cake\Collection\CollectionInterface $superhero
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Superhero'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="superhero index large-9 medium-8 columns content">
    <h3><?= __('Superhero') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
               <!--  <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('nickname') ?></th>
                <th scope="col"><?= $this->Paginator->sort('real_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('origin_description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('superpowers') ?></th>
                <th scope="col"><?= $this->Paginator->sort('catch_phrase') ?></th>
                <th scope="col"><?= $this->Paginator->sort('photo_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('photo_image') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($superhero as $superhero): ?>
            <tr>
               <!--  <td><?= $this->Number->format($superhero->id) ?></td> -->
                <td><?= h($superhero->nickname) ?></td>
                <td><?= h($superhero->real_name) ?></td>
                <td><?= h($superhero->origin_description) ?></td>
                <td><?= h($superhero->superpowers) ?></td>
                <td><?= h($superhero->catch_phrase) ?></td>
                <td><?= h($superhero->photo) ?></td>
                <td><?php echo $this->Html->image($superhero->photo); ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $superhero->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $superhero->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $superhero->id], ['confirm' => __('Are you sure you want to delete # {0}?', $superhero->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
