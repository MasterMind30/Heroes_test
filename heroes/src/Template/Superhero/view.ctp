<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Superhero $superhero
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Superhero'), ['action' => 'edit', $superhero->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Superhero'), ['action' => 'delete', $superhero->id], ['confirm' => __('Are you sure you want to delete # {0}?', $superhero->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Superhero'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Superhero'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="superhero view large-9 medium-8 columns content">
    <h3><?= h($superhero->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nickname') ?></th>
            <td><?= h($superhero->nickname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Real Name') ?></th>
            <td><?= h($superhero->real_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Origin Description') ?></th>
            <td><?= h($superhero->origin_description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Superpowers') ?></th>
            <td><?= h($superhero->superpowers) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Catch Phrase') ?></th>
            <td><?= h($superhero->catch_phrase) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Photo') ?></th>
            <td><?= h($superhero->photo) ?></td>
        </tr>
      
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($superhero->id) ?></td>
        </tr>
          <tr>
            <th scope="row"><?= __('Photo Dir') ?></th>
             <td><?php echo $this->Html->image($superhero->photo); ?></td>
        </tr>
    </table>
</div>
