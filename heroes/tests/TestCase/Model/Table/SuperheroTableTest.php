<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SuperheroTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SuperheroTable Test Case
 */
class SuperheroTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SuperheroTable
     */
    public $Superhero;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.superhero'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Superhero') ? [] : ['className' => SuperheroTable::class];
        $this->Superhero = TableRegistry::get('Superhero', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Superhero);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
