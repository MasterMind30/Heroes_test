-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2018 at 09:13 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `heroesdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `superhero`
--
create database heroesdb;

CREATE TABLE `superhero` (
  `id` int(11) NOT NULL,
  `nickname` varchar(100) NOT NULL,
  `real_name` varchar(100) NOT NULL,
  `origin_description` varchar(250) NOT NULL,
  `superpowers` varchar(250) NOT NULL,
  `catch_phrase` varchar(250) NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `photo_dir` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `superhero`
--

INSERT INTO `superhero` (`id`, `nickname`, `real_name`, `origin_description`, `superpowers`, `catch_phrase`, `photo`, `photo_dir`) VALUES
(31, 'Batman', 'Bruce Wayne', 'Bruce is brought up in Wayne Manor, with it\'s wealthy splendor, and lives a happy and privileged existence until the age of eight, when his parents are killed by a small-timed criminal named Joe Chill while on their way home from a movie theater', 'Money !!', 'it\'s Batman Time', 'Batman.jpg', NULL),
(32, 'Superman', 'Clark Kent', 'he was born Kal-El on the planet Krypton, before being rocketed to Earth as an infant by his scientist father Jor-El, moments before Krypton\'s destruction…', 'solar energy absorption and healing factor, solar flare and heat vision, solar invulnerability, flight…', 'Look, up in the sky, it\'s a bird, it\'s a plane, it\'s Superman!', 'superman.jpg', NULL),
(33, 'Wonder Woman', 'Diana Prince', 'Wonder Woman\'s origin story relates that she was sculpted from clay by her mother Queen Hippolyta and given life by Aphrodite, along with superhuman powers as gifts by the Greek gods', 'She possesses an arsenal of advanced technology, including the Lasso of Truth, a pair of indestructible bracelets, a tiara which serves as a projectile, and, in older stories, a range of devices based on Amazon technology.', 'Merciful Minerva!', 'wonder-woman.jpg', NULL),
(34, 'Green Lantern', 'Kyle Rayner', 'After the rest of the Corps was destroyed, he alone was selected to bear the last power ring and carry on the title', 'is semi-invulnerable, capable of projecting hard light constructions, flight, and utilizing various other abilities through his power ring which are only limited by his imagination and willpower.', 'In brightest day, in blackest night, no evil shall escape my sight. Let those who worship evil\'s might, beware my power, GREEN LANTERN\'S LIGHT!', 'greenlantern.jpg', NULL),
(35, 'Flash', 'Barry Allen', 'Barry Allen, a Silver Age forensic scientist who gained his powers when lightning struck at his lab and doused him with chemicals.', 'Using his super-speed powers, he taps into the Speed Force and becomes a costumed crime-fighter. His position is a legacy in the Flash Family, successor to the original Jay Garrick and predecessor to Wally West.', 'The fastest man alive', 'theflash.jpg', NULL),
(36, 'Cyborg', 'Victor Stone', 'Cyborg\'s origin was told via a medical journal read by Dr. Martin Stein saying Cyborg was a promising decathlon athlete until an accident destroyed most of his body and his father replaced part of his body with machine parts. Also, he is not a Titan.', 'Abilities. Large portions of Victor Stone\'s body have been replaced by advanced mechanical parts (hence the name Cyborg), granting him superhuman strength, speed, stamina, and flight.', 'i really dont know yet', 'cyborg.jpg', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `superhero`
--
ALTER TABLE `superhero`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `superhero`
--
ALTER TABLE `superhero`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
